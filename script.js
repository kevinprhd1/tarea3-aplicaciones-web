const wrapper = document.querySelector('.wrapper');
const loginLink = document.querySelector('.login-link');
const registerLink = document.querySelector('.register-link');
const btnPopup = document.querySelector('.btnLogin-popup');
const iconClose = document.querySelector('.icon-close');


registerLink.addEventListener('click', ()=>{
    wrapper.classList.add('active');

});

loginLink.addEventListener('click', ()=>{
    wrapper.classList.remove('active');
});


btnPopup.addEventListener('click', ()=>{
    wrapper.classList.add('active-popup');

});

iconClose.addEventListener('click', ()=>{
    wrapper.classList.remove('active-popup');

});

  var registerForm = document.getElementById('register-form');
  registerForm.addEventListener('submit', function(event) {
    event.preventDefault(); 
    
    window.location.href = 'home.html';
  });
  
  var loginForm = document.getElementById('login-form');
  loginForm.addEventListener('submit', function(event) {
    event.preventDefault(); 
    window.location.href = 'home.html';
  });
  

  